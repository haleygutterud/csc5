/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 5:19 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int average;
    int testscore1, testscore2, testscore3, total;
   
    cout << "Enter your test scores." << endl;
    cin >> testscore1 >> testscore2 >> testscore3;
    total = testscore1 + testscore2 + testscore3;
    average = total/3;
    cout << "Your average is: " << average << endl;
    
    return 0;
}

