/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2014, 5:07 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;


/*
 * 
 */
int main(int argc, char** argv) {
    string message;
    message = "Hello World";
    cout << "Hello, my name is Hal!" << endl; //output
    cout << "What is your name?" << endl; //output
    cin >> message; //user input's name
    cout << "Hello, " << message << ". I'm glad to meet you" << endl; //output
    return 0;
}

