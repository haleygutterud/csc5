/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 26, 2014, 4:46 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

int s(string words)
{
    return words.size();
}

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "Enter any word." << endl;
    string word;
    cin >> word;
    
    cout << "The word has: " << s(word) << " letters." << endl;

    return 0;
}

