/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 26, 2014, 4:43 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

double square(double number)
{
    return number * number;
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    cout << "Please enter a number." << endl;
    double num;
    cin >> num;
    
    cout << "The squared number is: " << square(num) << endl;

    return 0;
}

