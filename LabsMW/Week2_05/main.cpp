/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2014, 5:00 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int x, y;
    
    cout << "Please enter two numbers that are less than 1000." << endl;
    cin >> x >> y;
    int z = x;
    cout << "x = " << x << " y = " << y << endl;
    x = y;
    y = z;
    cout << "x = " << x << " y = " << y << endl;

    return 0;
}

