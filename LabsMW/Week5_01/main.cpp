/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 24, 2014, 4:39 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    //prompt user
    cout << "Please enter the account balance." << endl;
    int accountB;
    cin >> accountB;
    
    if (accountB >= 1000)
    {
        double interest = (1000 * .015) + .01 * (accountB - 1000);
        double total = interest + accountB;
        if (total <= 10)
        {
            double min = total;
            cout << "The minimum payment is: " << min << endl;
        }
        else
        {
            double min = total * .1;
            cout << "The minimum payment is: " << min << endl;
        }
    }
    else
    {
        double interest = accountB * .015;
        double total = interest + accountB;
        if (total <= 10)
        {
            double min = total;
            cout << "The minimum payment is: " << min << endl;
        }
        else
        {
            double min = total * .1;
            cout << "The minimum payment is: " << min << endl;
        }
        
    }

    return 0;
}

