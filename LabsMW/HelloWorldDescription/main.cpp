/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2014, 3:22 PM
 */

#include <cstdlib> // using cstdlib is unneeded
#include <iostream> // is for input and output from console

// std is short for standard
// all my libraries come from the standard C++ namespace
// Also defines content of keywords
using namespace std;

// In C++ white space is ignored

/*
 * 
 */
// All code is located inside main
// there is only one main per program
// code is executed top to bottom, left to right
//Argc and argv come from outside the program
int main(int argc, char** argv) {

    // variable definition
    // data type string
    // variable name is message
    //Assigning "Hello World" to message
    // string message = "Hello World";
    
    // All statement end in a semicolon
    //cout means console output
    // << is the stream operator
    // "Hello World" is a string literal
    // endl makes a new line
    // cout << "Hello World" << endl;
    
    string message; // variable declaration
    
    //Don't have to initialize message because
    //I am using cin to store a new value
    message = "Hello World"; // variable initiation
    
    // prompt the user
    cout << "Please enter a word." << endl;
    // Cin gets input from console and stored into the variable
    //message
    cin >> message;
    
    //output variable message
    cout << "You entered " << message << "." << endl;
    //If code gets here then it executed correctly

    cout << "Haley Gutterud" << endl;
    //Return flag
    return 0;
    
    //All code is inside main curly brackets
}

