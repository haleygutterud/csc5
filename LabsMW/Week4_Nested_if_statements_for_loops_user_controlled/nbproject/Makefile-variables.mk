#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=Cygwin_4.x-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/Cygwin_4.x-Windows
CND_ARTIFACT_NAME_Debug=week4_nested_if_statements_for_loops_user_controlled
CND_ARTIFACT_PATH_Debug=dist/Debug/Cygwin_4.x-Windows/week4_nested_if_statements_for_loops_user_controlled
CND_PACKAGE_DIR_Debug=dist/Debug/Cygwin_4.x-Windows/package
CND_PACKAGE_NAME_Debug=week4nestedifstatementsforloopsusercontrolled.tar
CND_PACKAGE_PATH_Debug=dist/Debug/Cygwin_4.x-Windows/package/week4nestedifstatementsforloopsusercontrolled.tar
# Release configuration
CND_PLATFORM_Release=Cygwin_4.x-Windows
CND_ARTIFACT_DIR_Release=dist/Release/Cygwin_4.x-Windows
CND_ARTIFACT_NAME_Release=week4_nested_if_statements_for_loops_user_controlled
CND_ARTIFACT_PATH_Release=dist/Release/Cygwin_4.x-Windows/week4_nested_if_statements_for_loops_user_controlled
CND_PACKAGE_DIR_Release=dist/Release/Cygwin_4.x-Windows/package
CND_PACKAGE_NAME_Release=week4nestedifstatementsforloopsusercontrolled.tar
CND_PACKAGE_PATH_Release=dist/Release/Cygwin_4.x-Windows/package/week4nestedifstatementsforloopsusercontrolled.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
