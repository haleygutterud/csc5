/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 12, 2014, 3:22 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Determine if a number is positive/negative
    //Determine if number is even/odd
//    cout << "Enter a number: " << endl;
//    int num;
//    cin >> num;
//    
//    //First way without nested statements
//    //Checking if pos/neg first
//    if (num < 0)
//    {
//        cout << "Negative number." << endl;
//    }
//    else 
//    {
//       cout << "Positive number." << endl;
//    }
//    
//    //Check if number is even or odd
//    if (num % 2 == 0)
//    {
//        cout << "Even number." << endl;
//    }
//    else
//    {
//        cout << "Odd number." << endl;
//    }
//    
//    //Nested statement
//    if (num < 0)
//    {
//        cout << "Negative number." << endl;
//        
//        if (num % 2 == 0)
//        {
//            cout << "Even number." << endl;
//        }
//        else
//        {
//            cout << "Odd number." << endl;
//        }
//    }
    
    //For loop
    //Iterate/loop 100 times. Output "i".
//    for(int i = 0; i < 100; i++)
//    {
//        cout << "i: " << i << endl;
//    }
    
    //User controlled loop
    //Prompt the user to enter a number
    //-1 to quit
    cout << "Enter a number. -1 to stop." << endl;
    int Usernum;
    cin >> Usernum;
    
    while (Usernum != -1)
    {
        //Output number to user
        cout << "You entered the number: " << Usernum << endl;
        
        //Prompt the user for another number
        cout << "Please enter another number. -1 to stop." << endl;
        cin >> Usernum;
    }
   
    return 0;
}

