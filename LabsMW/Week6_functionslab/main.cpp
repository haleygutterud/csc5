/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 26, 2014, 4:21 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

double value(int num1)
{
    return num1 * 3;
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    cout << "Enter a number." << endl;
    int number;
    cin >> number;
    
    cout << "The new number is: " << value(number) << endl;
    

    return 0;
}

