/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 4:17 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int meter;
    double mile, feet;
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    cout << "Please enter a measurement in meters." << endl;
    cin >> meter;
    mile = meter/1609.344;
    feet = meter * 3.281;
    cout << meter << " meters is " << mile << " miles" << " and " << feet << " feet." << endl;
    return 0;
}

