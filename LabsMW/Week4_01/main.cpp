/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 10, 2014, 4:07 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int x;
    
    cout << "Please enter your grade: " << endl;
    cin >> x;
    
    if (x <= 100 && x >= 90)
    {
        cout << "Your grade is: A" << endl;
    }
    else if (x < 90 && x >= 80)
    {
        cout << "Your grade is: B" << endl;
    }
    else if (x < 80 && x >= 70)
    {
        cout << "Your grade is: C" << endl;
    }
    else if (x < 70 && x >= 60)
    {
        cout << "Your grade is: D" << endl;
    }
    else
    {
        cout << "Your grade is: F" << endl;
    }
    

    return 0;
}

