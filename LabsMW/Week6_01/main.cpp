/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 26, 2014, 4:15 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

int max(int num1, int num2)
{
    if (num1 - num2 > 0)
    {
        return num1;
    }
    else
    {
        return num2;
    }
}

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Please enter two numbers." << endl;
    int number1;
    cin >> number1;
    int number2;
    cin >> number2;
    
    cout << "The greater value is: " << max(number1, number2) << endl;
    
    return 0;
}

