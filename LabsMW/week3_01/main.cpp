/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2014, 5:16 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int quarters, nickels, dimes, pennies, dollars;
    double owe, payment, change, need;
    
    //Get the amount owed
    cout << "Enter the amount owed: " << endl;
    cin >> owe;
    
    //Get payment
    cout << "Enter the amount of payment: " << endl;
    cin >> payment;
    
    if (payment > owe)
    {
        change = (payment - owe) * 100; //Convert to pennies
        dollars = change/100;
        quarters = (change - (dollars * 100))/25;
        dimes = (change - (dollars * 100) - (quarters * 25))/10;
        nickels = (change - (dollars * 100) - (quarters * 25) - (dimes * 10))/5;
        pennies = (change - (dollars * 100) - (quarters * 25) - (dimes * 10) - 
                (nickels * 5))/1;
        cout << "Your change is " << dollars << " dollars, " << quarters 
                << " quarters, " << dimes << " dimes, " << nickels 
                << " nickels, and " << pennies << " pennies." << endl;
    }
    else 
    {
        need = owe - payment; //What is still needed
        cout << "You still owe $" <<  fixed << 
                showpoint << setprecision(2) << need << endl;
    }

    return 0;
}

