/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 26, 2014, 3:34 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

//Function definition for the square function
//Function name is square
//One parameters, integer called num
//Returns an integer value
int square(int num)
{
    return num * num;
}
void problem1()
    {
        //invoke the square function
    cout << "Enter an integer." << endl;
    int value;
    cin >> value;
    
    //called the square function. Use value as argument
    int squareValue = square(value);
    
    cout << "The square of " << value << " is: " << squareValue << endl;
    cout << "The square of " << value << " is: " << square(value)<< endl;
    }
    
    //create end program function
    void endProgram()
    {
        cout << "Program is ending." << endl;
    }


/*
 * 
 */
int main(int argc, char** argv) {
    
    
    //invoke the square function
    cout << "Enter an integer." << endl;
    int value;
    cin >> value;
    
    //called the square function. Use value as argument
    int squareValue = square(value);
    
    cout << "The square of " << value << " is: " << squareValue << endl;
    cout << "The square of " << value << " is: " << square(value) << endl;
    
    
    int select;
    while(select != -1)
    {
        cout << "Enter a number." << endl;
        cout << "1 to run square." << endl;
        cout << "-1 to quit." << endl;
        cin >> select;
    //menu switch on value
        switch(select)
        {
            case 1:
                problem1();
                break;
            case -1:
                endProgram();
            default:
                cout << "Error" << endl;
        }
    }
    
    //create problem1 function
    
    return 0;
}
