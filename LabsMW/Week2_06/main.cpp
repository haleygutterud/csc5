/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2014, 5:12 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int x = 6;
    int y = 9;
    
    if (x ==7)
    {
        y = 10;
    }
    else
    {
        y = 3;
    }
    cout << "X: " << x << " Y: " << y << endl;

    return 0;
}

