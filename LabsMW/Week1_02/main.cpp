/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2014, 4:18 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int a = 5;
    int b = 10;
    
    //create a new variable to temporarily store my data
    int temp = a;
    
    cout << "a: " << a << "b: " << b << endl;
    
    a = b;
    b = temp;
    
    cout << "a: " << a << "b: " << b << endl;
    

    return 0;
}

