/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2014, 4:39 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int singles, doubles, triples, homeruns, at_bats;
    double slugging_percentage;
    
    cout << "Please enter your singles." << endl;
    cin >> singles;
    cout << "Please enter your doubles." << endl;
    cin >> doubles;
    cout << "Please enter your triples." << endl;
    cin >> triples;
    cout << "Please enter your homeruns." << endl;
    cin >> homeruns;
    cout << "Please enter your at bats." << endl;
    cin >> at_bats;
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    slugging_percentage = (singles + (2 * doubles) + 
            (3 * triples) + (4 * homeruns))/at_bats;
    cout << "Your slugging percentage is: " << slugging_percentage << endl;

    return 0;
}

